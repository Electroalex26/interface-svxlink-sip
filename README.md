# Interface SVXLink SIP

Tutoriel résumé de tests

## Sommaire

* Installation (simple) d'Asterisk
* Extra : extension pour écouter le RRF
* Installation de SVXReflector
* Installation de SVXLink avec module SIP
* Configuration de l'interconnexion
* Utilisation

## Installation (simple) d'Asterisk

### Installation

Installation depuis les dépôts :

```sh
sudo apt update
sudo apt upgrade
sudo apt install asterisk -y
```

Asterisk est désormais installé, il nous faut maintenant paramétrer quelques choses importantes. Nous faisons d'abord un backup des fichiers de configuration par défaut.

>Les commandes manoeuvrant des fichiers de configuration sont à executer avec les droits root.

```sh
sudo mv /etc/asterisk/users.conf /etc/asterisk/users.conf.old
sudo mv /etc/asterisk/extensions.conf /etc/asterisk/extensions.conf.old
```

### users.conf

Nous configurons d'abord les utilisateurs

```sh
sudo nano /etc/asterisk/users.conf
```

Nous utiliserons une structure simple de configuration des utilisateurs qui est la suivante :

```conf
[User-id]
fullname = User-name
secret = User-password
hassip = yes
context = users
host = dynamic
```

* L'ID est le numéro d'identification de l'utilisateur
* fullname est le nom va s'affichera (il a peu d'importance dans l'authentification)
* secret est le mot de passe de l'utilisateur
* hassip précise qu'il s'agit d'un compte SIP
* context spécifie qu'il s'agit bien d'un profil de type ```users```
* host précise que notre utilisateur n'a pas d'IP spécifique (permet une connexion sans prise de tête lorsque l'IP du téléphone SIP ou du softphone n'est pas fixe)

Pour la suite nous utiliserons le fichier de configuration suivant :

```conf
[6001]
fullname = VoIP-user
secret = 1234
hassip = yes
context = users
host = dynamic

[6002]
fullname = SVX-interface
secret = 1234
hassip = yes
context = users
host = dynamic
```

Nous pouvons désormais appliquer les changements en nous connectant à la console d'Asterisk :

```sh
sudo asterisk -rvvv
```

Et nous exécutons dans cette console la commande suivante :

```sh
reload
```

Nous pouvons ensuite vérifier que nos utilisateurs ont bien été créé :

```sh
sip show users
```

Le résultat attendu est le suivant :

```
Username                   Secret           Accountcode      Def.Context      ACL  Forcerport
6002                       1234                              users            No   No        
6001                       1234                              users            No   No        
```

Nous pouvons maintenant quitter la console Asterisk avec un simple ```Ctrl+C```.

__Il faut désormais connecter un téléphone SIP ou un SoftPhone au serveur avec l'utilisateur VoIP-user. Je ne vais pas détailler cela ici, ça dépend beaucoup du téléphone ou logiciel utilisé. Les infos ci-dessous peuvent vous aider__
* Serveur SIP et serveur d'enregistrement : l'IP de la machine hébergeant Asterisk
* Port : 5060 (par défaut)
* ID utilisateur et ID d'authentification : 6001
* Mot de passe d'authentification : 1234

Si tout s'est bien passé vous devriez voir dans la console Asterisk le message suivant :

```
   -- Registered SIP '6001' at ip-du-telephone:5060
```

Théoriquement à ce niveau votre téléphone est correctement connecté et prêt à être utilisé. Mais pour l'instant si vous tapez un numéro, vous allez simplement avoir en réponse : ```404 Non trouvé``` ! Il faut maintenant configurer les extensions.

### extensions.conf

Nous modifions alors maintenant ce second fichier de configuration :

```sh
sudo nano /etc/asterisk/extensions.conf
```

On peut alors configurer plusieurs extensions, nous allons commencer par les plus classiques. Elle serviront à pouvoir faire communiquer les utilisateurs entre eux quand ils taperont l'ID de l'autre utilisateur

```conf
[users]
exten => 6001,1,Dial(SIP/6001)
exten => 6002,1,Dial(SIP/6002)
```

A partir de maintenant, si l'on compose le 6002, le serveur va appeler en utilisant le protocole SIP l'ID 6002. Et vice versa.

S'en est tout pour la configuration d'Asterisk !

Inspiré (à 100%) du [Mike's Software Blog](https://mike42.me/blog/2015-01-02-how-to-set-up-asterisk-in-10-minutes).

## Extra : extension pour écouter le RRF

L'extension suivante permet d'écouter le RRF depuis le téléphone :

```conf
; SVXLINK RRF
exten => 4000,1,Answer
 same => n,Wait(1)
 same => n,MP3Player(http://rrf.f5nlg.ovh/audiostream/RRF)
 same => Hangup()
```

Attention, il peut-être requis d'installer le paquet suivant ```libsox-fmt-mp3```. Il suffit de taper la commande suivante :

```sh
sudo apt install libsox-fmt-mp3
```

Désormais lorsque l'on compose le ```4000``` le serveur nous joue le flux audio disponible sur le dashboard.
Vous pouvez ajouter des extensions similaires pour d'autres réseaux SVXLink.

Merci [@isithran](https://twitter.com/isithran) pour l'astuce à retrouver [en détails ici](https://twitter.com/isithran/status/1262449885858664448).

## Installation de SVXReflector

Vous pouvez là installer la version officielle de SVXLink de SM0SVX ou le fork intégrant le module SIP de DL1HRC, cela n'a pas d'importance. 
Pour ma part j'ai dû le compiler, la version dispo dans les dépôt n'intègre pas SVXReflector.

Pour cela, il suffit de suivre les instructions que l'on peut trouver ici : [Installation from source](https://github.com/sm0svx/svxlink/wiki/InstallationInstructions#installation-from-source).

N'hésitez pas à installer la liste de dépendances proposées ici avant, ça vous évitera de devoir les installer une à une : [How to install SVXReflector](https://github.com/sm0svx/svxlink/wiki/How-to-install-SvxReflector%3F).

Une fois l'installation finie, vous pouvez modifier le fichier de configuration :

```sh
sudo nano /etc/svxlink/svxreflector.conf
```

Il n'y a pas beaucoup de modifications à lui apporter. Il s'agit surtout de spécifier le port à écouter, le nom des utilisateurs autorisés et leur mot de passe attribué.

```conf
[GLOBAL]
TIMESTAMP_FORMAT="%d.%m.%Y %H:%M:%S"
LISTEN_PORT=5000
TG_FOR_V1_CLIENTS=999

[USERS]
CLIENT=default
SVX-interface=default

[PASSWORDS]
default="MOT_de_PASSE"
```

Vous devriez désormais pouvoir démarrer SVXReflector en tapant la commande suivante :

```sh
svxreflector
```

Il peut être intéressant de créer un daemon qui se charge de son éxecution : _A compléter_.

## Installation de SVXLink avec module SIP

Le module SIP n'a pas été intégré à la version "officielle" de SVXLink, il s'agit là d'un fork de DL1HRC.

### PjSIP

Ceci ne doit pas être installé sur la même machine qu'Asterisk : les deux sont incompatibles.

Téléchargez d'abord la dernière archive dans le format de votre souhait.
Il faut ensuite l'extraire, si vous utilisez le ```.tar.gz``` la commande suivante peut-être utilisée :

```sh
tar xfzv nom-du-fichier.tar.gz
```

Nous allons ensuite le compiler et l'installant :

```sh
cd pjproject*
./configure --disable-sound --disable-video --disable-libwebrtc
make dep
make
sudo make install
```

### SVXLink

Nous allons maintenant compiler la version modifiée de SVXLink. Pour cela il est conseillé d'installer les dépendances comme proposé lors de l'installation du reflector. Ensuite nous exécutons les commandes suivantes :

```sh
git clone https://github.com/dl1hrc/svxlink.git
cd svxlink
git checkout pjSipLogic
cd src
mkdir build; cd build
cmake -DUSE_QT=OFF -DCMAKE_INSTALL_PREFIX=/usr -DSYSCONF_INSTALL_DIR=/etc -DLOCAL_STATE_DIR=/var -DCMAKE_BUILD_TYPE=Release ..
make
sudo make install
```

Afin d'éviter les erreurs, il convient d'ajouter les fichiers audios à l'installation. Pour cela exécuter les commandes suivantes :

```sh
cd /usr/share/svxlink/sounds
sudo git clone https://github.com/dl1hrc/svxlink-sounds-en_US-heather.git
sudo ln -s svxlink-sounds-en_US-heather en_US
```

On effectue une sauvegarde du fichier de configuration par défaut :

```sh
sudo mv /etc/svxlink/svxlink.conf /etc/svxlink/svxlink.conf.old
```

Inspiré (à 100%) de [DL1HRC presentation](https://forum.funk-telegramm.de/thread/806-sip-erweiterung-f%C3%BCr-svxlink/).

## Configuration de l'interconnexion

Il va maintenant falloir modifier le fichier de configuration du client SVXLink qui fera l'interconnexion avec les deux réseaux :

```sh
sudo nano /etc/svxlink/svxlink.conf
```

```conf
[GLOBAL]
LOGICS=ReflectorLogic,SipLogic
CFG_DIR=svxlink.d
TIMESTAMP_FORMAT=%c
CARD_SAMPLE_RATE=16000
CARD_CHANNELS=1
LINKS=NetLink


[NetLink]
CONNECT_LOGICS=ReflectorLogic,SipLogic
DEFAULT_ACTIVE=1
TIMEOUT=0

[ReflectorLogic]
TYPE=Reflector
CALLSIGN=SVX-interface ;*
AUDIO_CODEC=OPUS
JITTER_BUFFER_DELAY=2
HOST=ip-svxreflector ;*
AUTH_KEY=MOT_de_PASSE ;*
PORT=5000 ;*
DEFAULT_TG=999
MONITOR_TGS=999
EVENT_HANDLER=/usr/share/svxlink/events.tcl
DEFAULT_LANG=en_US

[SipLogic]
TYPE=Sip
MODULES=ModuleHelp,ModuleParrot,ModuleEchoLink
SIPSERVER=ip-asterisk ;*
SIPPORT=5060
USERNAME=6002 ;*
CALLERNAME="SVX-interface"
PASSWORD=1234 ;*
SIPSCHEMA=Digest
SIPEXTENSION=default
SIPREGISTRAR=ip-asterisk ;*
AUTOANSWER=1
#AUTOCONNECT=621
SIP_LOGLEVEL=3
CALL_TIMEOUT=45
SEMI_DUPLEX=0
SIP_CTRL_PTY=/tmp/sip_pty
VOX_FILTER_DEPTH=20
VOX_THRESH=1000
SQL_START_DELAY=0
SQL_DELAY=0
SQL_HANGTIME=1800
#REG_TIMEOUT=300
JITTER_BUFFER_DELAY=300
EVENT_HANDLER=/usr/share/svxlink/events.tcl
DTMF_CTRL_PTY=/tmp/dtmf
DEFAULT_LANG=en_US
```

Les lignes marquées d'un * sont à adapter à vos configurations précédentes.

Vous devriez désormais pouvoir démarrer SVXLink en tapant la commande suivante :

```sh
svxreflector
```

Il peut être intéressant de créer un daemon qui se charge de son éxecution : _A compléter_.

Si tout s'est bien passé vous devriez voir dans la console Asterisk le message suivant :

```sh
   -- Registered SIP '6002' at ip-du-telephone:5060
```

Théoriquement à ce niveau votre interconnexion est opérationnelle et prête à être utilisé

## Utilisation

Désormais il suffit d'appeler depuis le téléphone SIP le ```6002``` pour se connecteur au réseau SVXLink.

Pour parler, comme il s'agit de VOX, il s'agit de parler !

Et puis il faudra surement résoudre quelques bugs, pour ça bonne chance ! ^^'